package com.preeya.week08;

public class Circle {
    private double radius;
    private Double areaCircle;
    private double perimeterCircle;
    public Circle(double d) {
        this.radius = d;
    }
    public Double printCircleArea() {
        areaCircle = (Double) (3.14 * (radius * radius));
        System.out.println("Area of Circle = "+ areaCircle);
        return areaCircle;
    }
    public Double printCirclePerimeter() {
        perimeterCircle = 2 * (3.14 * radius);
        System.out.println("Perimeter of Circle = "+ perimeterCircle);
        return perimeterCircle;
    }
}