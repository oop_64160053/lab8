package com.preeya.week08;

public class Triangle {
    private int a = 5;
    private int b = 5;
    private int c = 6;
    private int s = 0;
    private int areatriangle;

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public int printTrianglePerimeter() {
        s = (a+b+c) / 2;
        System.out.println("Perimeter of triangle = "+ s);
        return s;
    }
    public int printTriangleArea() {
        s = (a+b+c) / 2;
        areatriangle = (int) Math.sqrt(s * (s-a) * (s-b) * (s-c));
        System.out.println("Area of triangle = "+ areatriangle);
        return areatriangle ;
    }

}