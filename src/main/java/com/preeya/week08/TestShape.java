package com.preeya.week08;

public class TestShape {
        public static void main(String[] args) {
            Rectangle rectangle1 = new Rectangle(10,5);
            rectangle1.printRectangleArea1();
            rectangle1.printRectanglePerimeter1();
            Rectangle rectangle2 = new Rectangle(5,3);
            rectangle2.printRectangleArea2();
            rectangle2.printRectanglePerimeter2();
    
    
            Circle circle1 = new Circle(16);
            circle1.printCircleArea();
            circle1.printCirclePerimeter();
            Circle circle2 = new Circle(8.4);
            circle2.printCircleArea();
            circle2.printCirclePerimeter();

            Triangle triangle1 = new Triangle(5, 5, 6);
            triangle1.printTriangleArea();
            Triangle perimeter = new Triangle(5, 5, 6);
            perimeter.printTrianglePerimeter();
    
    }
    
}
 
    