package com.preeya.week08;

public class Map {
    private int width;
    private int height;

    public Map(){
        this.width = 5;
        this.height = 5;

    }
    public Map(int  x,int y){
        this.width = x;
        this.height = y;

    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    void print(){
        for(int i=0; i<width;i++){
            for(int j=0;j<height;j++){
                System.out.print("-");

            }
            System.out.println();
        }
    }
}
