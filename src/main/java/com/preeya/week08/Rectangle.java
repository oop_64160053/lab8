package com.preeya.week08;

public class Rectangle {
    private int height;
    private int width;
    private int area;
    private int perimeter;
    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }
    public int printRectangleArea1() {
        area = height * width;
        System.out.println("Area of rectangle1 = "+ area);
        return area;
    }
    public int printRectangleArea2() {
        area = height * width;
        System.out.println("Area of rectangle2 = "+ area);
        return area;
    }
    public int printRectanglePerimeter1() {
        perimeter = (height * 2) + (width * 2);
        System.out.println("Perimeter of rectangle1 = "+perimeter);
        return perimeter;
    }
    public int printRectanglePerimeter2() {
        perimeter = (height * 2) + (width * 2);
        System.out.println("Perimeter of rectangle2 = "+perimeter);
        return perimeter;
    }
}